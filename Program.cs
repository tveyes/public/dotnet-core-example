using System;
using System.Threading;
using NLog;

namespace dotnetcore
{
    class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            logger.Debug("Starting to debug...");
            Console.WriteLine("This will loop forever...");
            while(true) {
                logger.Info("Hello World!!! (x 100)");
                Thread.Sleep(2000);
            }
        }
    }
}
