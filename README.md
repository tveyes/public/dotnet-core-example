## Introduction

This is a simple pipeline example for a .NET Core application, showing just
how easy it is to get up and running with .NET development using GitLab.

This pipeline builds and tests .net core code and builds and deploys docker images to the gitlab docker registry.
